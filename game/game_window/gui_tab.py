from libtcodpy164 import *
from game_object import item
from config import conf


class GuiTab:
    """
    Generic tab on the left side of the screen, that can show character sheet, inventory, equipment, etc.
    """
    console = console_new(conf.gui_tabs_width, conf.gui_tabs_height)
    offset_x = 0
    offset_y = conf.top_bar_height
    foreground = white
    background = black
    border_colour = white

    def __init__(self, player, mouse):
        console_set_default_foreground(self.console, self.foreground)
        console_set_default_background(self.console, self.background)
        self.player = player
        self.mouse = mouse
        self.blit_params = {
            'src': self.console,
            'x': 0,
            'y': 0,
            'w': 0,
            'h': 0,
            'dst': 0,
            'xdst': self.offset_x,
            'ydst': 0,
            'ffade': 1.0,
            'bfade': 0.3,
        }
        self.corners = {}
        self.titles = {
            'cha': '+Character+',
            'inv': '+Inventory+',
            'eqp': '+Equipment+',
            'spb': '+Spellbook+',
        }

    def _draw_borders(self):
        """

        :return:
        """
        self._draw_h_borders()
        self._draw_v_borders()
        self._draw_corners()

    def _draw_v_borders(self):
        """
        Draws vertical borders.

        :return:
        """
        vertical_borders = [(x, y) for x in (0, conf.gui_tabs_width - 1)
                            for y in range(2, conf.gui_tabs_height - 1)]
        for (x, y) in vertical_borders:
            console_put_char_ex(self.console, x, y, 186, self.border_colour, self.background)

    def _draw_h_borders(self):
        """
        Draws horizontal borders.

        :return:
        """
        horizontal_borders = [(x, y) for x in range(1, conf.gui_tabs_width - 1)
                              for y in (2, conf.gui_tabs_height - 1)]
        for (x, y) in horizontal_borders:
            console_put_char_ex(self.console, x, y, 205, self.border_colour, self.background)

    def _draw_tag(self, tag_width, tag_name):
        """
        Draws the tag in the top

        :type tag_width: list
        :param tag_width: range of x coordinates denoting width of the tag
        :type tag_name: str
        :param tag_name: non-capitalised tag name
        :return:
        """
        for x in tag_width:
            console_put_char_ex(self.console, x, 0, chr(205), self.border_colour, black)
        console_put_char_ex(self.console, tag_width[-1], 0, chr(187), self.border_colour, black)
        console_put_char_ex(self.console, tag_width[-1], 1, chr(186), self.border_colour, black)
        console_put_char_ex(self.console, tag_width[0], 0, chr(201), self.border_colour, black)
        console_put_char_ex(self.console, tag_width[0], 1, chr(186), self.border_colour, black)
        console_print_ex(self.console, tag_width[1], 1, BKGND_NONE, LEFT, tag_name.capitalize())

    def _draw_corners(self):
        """
        Draws the fancy corners of the equipment.

        :return:
        """
        for xy in self.corners:
            x, y = xy
            console_put_char_ex(self.console, x, y, self.corners[xy], self.border_colour, self.background)

    def _highlight_item(self, mouseunder_item):
        """
        Highlights the background of an item

        :type mouseunder_item: item.Item
        :param mouseunder_item: item under the mouse cursor
        """
        adjusted_mouse_xy = (self.mouse.cx - self.offset_x, self.mouse.cy - self.offset_y)
        adjusted_x, adjusted_y, item_xy = self._determine_eqp_or_inv(mouseunder_item)
        bkgnd_params = {
            'con': self.console,
            'x': adjusted_x,
            'y': adjusted_y,
            'col': lighter_green,
        }
        if adjusted_mouse_xy == item_xy:
            console_set_char_background(**bkgnd_params)
            self._item_interaction(mouseunder_item)

    def _determine_eqp_or_inv(self, mouseunder_item):
        """
        Finds out if the item shown in the tab is a piece of equipment, or just an item.

        :param mouseunder_item:
        :return:
        """
        if isinstance(mouseunder_item, item.Equipment):
            item_xy = (mouseunder_item.eqp_x, mouseunder_item.eqp_y)
            adjusted_x = mouseunder_item.eqp_x + self.offset_x
            adjusted_y = mouseunder_item.eqp_y + self.offset_y
        else:
            item_xy = (mouseunder_item.inv_x, mouseunder_item.inv_y)
            adjusted_x = mouseunder_item.inv_x + self.offset_x
            adjusted_y = mouseunder_item.inv_y + self.offset_y
        return adjusted_x, adjusted_y, item_xy

    def _item_interaction(self, clicked_item):
        """
        Method defined inside GuiTab for the purposes of _highlight_item.
        It is overridden below for the particular behavior of inventory and equipment tabs.

        :param clicked_item:
        :return:
        """
        pass

    def blit(self, tag_width, tag_name):
        """
        Blits the inventory panel to the root console.

        :return:
        """
        title_params = {
            'con': self.console,
            'x': conf.gui_tabs_width / 2,
            'y': 1 + self.offset_y,
            'flag': BKGND_NONE,
            'alignment': CENTER,
            'fmt': self.titles[tag_name],
        }
        console_print_ex(**title_params)
        self._draw_borders()
        self._draw_tag(tag_width, tag_name)
        console_blit(**self.blit_params)
        console_clear(self.console)
