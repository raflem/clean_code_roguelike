from setuptools import setup

setup(name='game',
      version='0.3',
      description='The funniest joke in the world',
      url='https://gitlab.com/raflem/clean_code_roguelike',
      author='Rafal A. Lemiec',
      author_email='rafalemiec@tuta.io',
      license='MIT',
      packages=[
            'dungeon_builder',
            'game_map',
            'game_object',
            'game_room',
            'game_window',
      ],
      zip_safe=False)
