import tcod


class Tile:
    """
    A basic building block of the map

    :return:
    """
    name = 'tile'
    char = 0
    transparent = False

    def __init__(self, console):
        self.console = console
        self.walkable = False
        self.explored = False
        self.foreground_color = tcod.white
        self.background_color = tcod.black

    def draw(self, x, y):
        """
        Draws the tile on the console.

        :param x: horizontal position on the screen
        :param y: vertical position on the screen
        """
        tcod.console_put_char_ex(self.console, x, y, self.char, self.foreground_color, self.background_color)
