import tcod
from .g_obj import GameObject
from random2 import randint
from config import conf


class Item(GameObject):
    """
    Items can be picked up and then used
    """

    def __init__(self):
        GameObject.__init__(self)
        self.inv_key = None
        self.inv_x, self.inv_y = 0, 0

    def spawn(self, position, level, container):
        """
        Spawns the character, assigning it a place on the level, adding it to the container,
        and blocking sight of the tile, if needed.

        :type position: tuple
        :type level: list
        :param level: level on which the character is represented
        :param container: collection of all game objects
        """
        self.x, self.y = position
        self.game_objects = container
        self.game_objects.append(self)
        self.level = level

    def add_to_inventory(self, inventory, container):
        if self._inventory_slot_available(inventory):
            self.game_objects = container
            self.inv_x, self.inv_y = conf.inv_xy[self.inv_key]
            inventory[self.inv_key] = self

    def pick_up(self, inventory):
        if self._inventory_slot_available(inventory):
            self.inv_x, self.inv_y = conf.inv_xy[self.inv_key]
            self.game_objects.remove(self)
            inventory[self.inv_key] = self

    def drop(self, character):
        character.inventory[self.inv_key] = None
        self.spawn((character.x, character.y), self.level, self.game_objects)

    def use(self, player):
        player.inventory[self.inv_key] = None

    def _inventory_slot_available(self, inventory):
        """
        Returns the dict key of the free inventory slot

        :type inventory: dict
        :param inventory: player's inventory
        :rtype: int
        :return: item's key in inventory
        """
        for item_key in inventory:
            if not inventory[item_key]:
                self.inv_key = item_key
                return True


class Equipment(Item):
    def __init__(self):
        Item.__init__(self)
        self.slot = None
        self.eqp_x, self.eqp_y = 0, 0

    def equip(self, player):
        if player.equipment[self.slot] is None:
            self.eqp_x, self.eqp_y = conf.eqp_xy[self.slot]
            player.equipment[self.slot] = self
            player.inventory[self.inv_key] = None

    def dequip(self, player):
        if self._inventory_slot_available(player.inventory):
            player.equipment[self.slot] = None
            self.inv_x, self.inv_y = conf.inv_xy[self.inv_key]
            player.inventory[self.inv_key] = self


class Corpse(Item):
    """
    Something died.
    """
    def __init__(self, name):
        Item.__init__(self)
        self.name = "{}'s corpse".format(name)
        self.char = 38
        self.color = tcod.red


class HealingPotion(Item):
    """
    Potions are for drinking
    """
    def __init__(self):
        Item.__init__(self)
        self.name = 'healing potion'
        self.char = 173
        self.color = tcod.purple
        self.value = randint(3, 5)

    def use(self, player):
        """
        Uses the item

        :return:
        """
        player.inventory[self.inv_key] = None
        player.stats['hp'] += self.value


class Sword(Equipment):
    def __init__(self):
        Equipment.__init__(self)
        self.name = 'sword'
        self.char = 47
        self.color = tcod.silver
        self.slot = 3  # weapon


class Ring(Equipment):
    def __init__(self):
        Equipment.__init__(self)
        self.name = 'simple ring'
        self.char = 'o'
        self.color = tcod.gold
        self.slot = 8  # ring
