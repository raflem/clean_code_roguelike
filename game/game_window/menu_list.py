from config import conf
from libtcodpy164 import *


class MenuList:
    def __init__(self, header, options):
        """
        Generic menu list, for handling pauses and such

        :param header: header text
        :type header: str
        :param options: list of selectable options
        :type options: list
        """
        self.options = options
        self.width = len(max(options, key=len)) + 4
        self.height = len(options) + 4

        self.header = header
        self.header_align = CENTER
        self.list_align = LEFT
        self.offset_x = 1

        self.console = console_new(self.width, self.height)
        self.letters_colour = black
        self.background_colour = white
        console_set_default_foreground(self.console, self.letters_colour)
        console_set_default_background(self.console, self.background_colour)

    def blit(self):
        """
        Blits the contents of the menu list console to the root console.

        :return:
        """
        console_clear(self.console)
        console_print_ex(self.console, self.width / 2, 1, BKGND_NONE, self.header_align, self.header)
        self.list_options()
        menu_list_kwargs = {
            'src': self.console,
            'x': 0,  # x from which blitting starts
            'y': 0,  # y from which blitting starts
            'w': 0,  # width cut from the source console
            'h': 0,  # height cut from the source console
            'dst': 0,  # destination console; 0 is for root
            'xdst': (conf.window_width - self.width) / 2,  # horizontal offset
            'ydst': conf.camera_height / 2,  # vertical offset
            'ffade': 1.0,  # foreground opacity
            'bfade': 0.7,  # background opacity
        }
        console_blit(**menu_list_kwargs)

    def list_options(self):
        """
        Lists all the options inside the menu rectangle, one under the other.

        :return:
        """
        offset_y = 3
        letter_index = ord('a')
        for option in self.options:
            options_params = {
                'con': self.console,
                'x': self.offset_x,
                'y': offset_y,
                'flag': BKGND_NONE,
                'alignment': self.list_align,
                'fmt': '{}_{}'.format(chr(letter_index), option),
            }
            console_print_ex(**options_params)
            letter_index += 1
            offset_y += 1

