from ..game_map.floor import Floor
from random2 import randint


class Corridor:
    def __init__(self, start_x, start_y):
        self.x = start_x
        self.y = start_y
        self.console = None
        self.level = None
        self.container = None

    def set_params(self, console, level, container):
        """
            Used to set the console and level tileset, used by the GameMap

            :param console:
            :param container:
            :param level:
            """
        self.console = console
        self.level = level
        self.container = container

    def dig_any_direction(self, end_x, end_y):
        """

        :type end_x: int
        :type end_y: int
        :return:
        """
        coin = randint(0, 1)
        if coin == 0:
            self.dig_vertical_first(end_x, end_y)
        else:
            self.dig_horizontal_first(end_x, end_y)

    def dig_horizontal_first(self, end_x, end_y):
        for x in range(min(self.x, end_x), max(self.x, end_x) + 1):
            self.level[x][self.y] = Floor(self.console)
        for y in range(min(self.y, end_y), max(self.y, end_y) + 1):
            self.level[end_x][y] = Floor(self.console)
        print("start_x:{}|end_x:{}|start_y:{}|end_y:{}".format(self.x, end_x, self.y, end_y))

    def dig_vertical_first(self, end_x, end_y):
        for y in range(min(self.y, end_y), max(self.y, end_y) + 1):
            self.level[self.x][y] = Floor(self.console)
        for x in range(min(self.x, end_x), max(self.x, end_x) + 1):
            self.level[x][end_y] = Floor(self.console)
