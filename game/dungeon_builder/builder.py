from ..game_object import Goblin
from ..game_room import Rectangle, Square, Corridor
from random2 import randint, randrange

from config import conf


class DungeonBuilder:
    def __init__(self, console, level, game_obj_container, events):
        """
        Responsible for creating the dungeon.

        :param console: map console, where the tiles are rendered
        :param level: matrix of tiles that are being manipulated
        :type level: list
        :param game_obj_container: list of game objects where to add enemies, furniture, loot, etc.
        :type game_obj_container: list
        """
        self.console = console
        self.level = level
        self.game_objects = game_obj_container
        self.events = events
        self.room_params = {
            'console': self.console,
            'level': self.level,
            'container': self.game_objects,
        }

    def build_tower(self, game):
        """
        Builds the level from rooms

        :param game: game instance
        """
        sides = [
            'top', 'bot', 'left', 'right'
        ]
        self.build_central_room(sides, game)
        self.build_corridors()
        self.build_main_rooms()

    def build_random(self, max_rooms, game):
        """
        Creates a dungeon from several randomised rooms

        :param game: game instance
        :type max_rooms: int
        :return:
        """
        print("Building random dungeon from {} rooms".format(max_rooms))
        _rooms = []
        for room in range(max_rooms):
            width, height = randint(5, 8), randint(5, 8)
            start_x, start_y = randint(1, conf.level_width - width - 1), randint(1, conf.level_height - height - 1)

            new_room = Rectangle(width, height)
            new_room.set_params(**self.room_params)
            new_room.set_corners(start_x, start_y)

            start_x, start_y = self.__collision_checker(width, height, start_x, start_y, new_room, _rooms)
            new_room.dig_out(start_x, start_y)
            _rooms.append(new_room)

            cent_x, cent_y = new_room.center_x, new_room.center_y
            self.__make_corridors(cent_x, cent_y, game, _rooms)
            self.__populate(cent_x, cent_y)
        print("Finished with {} rooms".format(len(_rooms)))

    def __make_corridors(self, cent_x, cent_y, game, rand_rooms):
        """
        Creates corridors, connecting the current room to the previous one.

        :type cent_x: int
        :param cent_x: current room's center x
        :type cent_y: int
        :param cent_y: current room's center y
        :param game: game instance
        :type rand_rooms: list
        :param rand_rooms: list of rooms
        :return:
        """
        if len(rand_rooms) > 1:
            prev_x, prev_y = rand_rooms[-2].center_x, rand_rooms[-2].center_y
            self._dig_corridors(cent_x, cent_y, prev_x, prev_y)
        else:
            game.spawn_player(cent_x, cent_y)

    @staticmethod
    def __collision_checker(width, height, start_x, start_y, new_room, rooms_list):
        """
        Checks if the current room overlaps any already created room.

        :type width: int
        :type height: int
        :type start_x: int
        :type start_y: int
        :type new_room: Rectangle
        :type rooms_list: list
        :return:
        """
        for prev_room in rooms_list:
            if new_room.intersects_other(prev_room):
                print(">>>collision<<<\nretrying room {}".format(len(rooms_list) + 1))
                start_x = randint(1, conf.level_width - width - 1)
                start_y = randint(1, conf.level_height - height - 1)
        return start_x, start_y

    def _dig_corridors(self, cent_x, cent_y, prev_x, prev_y):
        corridor = Corridor(cent_x, cent_y)
        corridor.set_params(**self.room_params)
        corridor.dig_any_direction(prev_x, prev_y)

    def __populate(self, x, y):
        """
        Spawns NPCs in the game.
        """
        for some_int in range(randrange(0, 4)):
            spawn_x, spawn_y = x + randint(0, 3), y + randint(0, 3)
            enemy = Goblin(self.game_objects, self.events)
            enemy.spawn((spawn_x, spawn_y), self.level)

    def build_main_rooms(self):
        """
        Builds big corner rooms.
        """
        main_room = Square(11)
        main_room.set_params(**self.room_params)
        rooms_xy = {
            (1, 1): ['bot', 'right'],
            (21, 1): ['bot', 'left'],
            (21, 21): ['top', 'left'],
            (1, 21): ['top', 'right'],
        }

        for xy in rooms_xy:
            room_x, room_y = xy
            door_sides = rooms_xy[xy]
            main_room.dig_out(room_x, room_y)
            for side in door_sides:
                main_room.make_doors(side)

    def build_corridors(self):
        """
        Creates the corridors, stretching outwards from the middle

        :return:
        """
        corridor_vert = Rectangle(7, 11)
        corridor_vert.set_params(**self.room_params)
        corridor_hor = Rectangle(11, 7)
        corridor_hor.set_params(**self.room_params)
        corridors_xy = [(13, 1), (1, 13), (13, 21), (21, 13)]

        for xy in corridors_xy:
            room_x, room_y = xy
            if corridors_xy.index(xy) % 2 == 0:
                corridor_vert.dig_out(room_x, room_y)
            else:
                corridor_hor.dig_out(room_x, room_y)
            self.__populate(room_x, room_y)

    def build_central_room(self, sides, game):
        """
        Creates the central hub room

        :param game:
        :param sides:
        """
        central_room = Square(7)
        central_room.set_params(**self.room_params)
        central_room.dig_out(13, 13)
        central_room.build_central_stairs()
        game.spawn_player(central_room.center_x, central_room.center_y)

        if randint(0, 1) == 1:
            central_room.build_central_pillars()
        else:
            central_room.build_central_hub()
        for side in sides:
            central_room.make_doors(side)

