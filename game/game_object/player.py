import tcod

from .g_char import GameCharacter
from .item import Item
from .furniture import UpStairs, DownStairs


class Player(GameCharacter):
    directions = {
        43: (1, -1),  # up-right
        42: (0, -1),  # up
        41: (-1, -1),  # up-left
        40: (1, 0),  # right
        38: (-1, 0),  # left
        37: (1, 1),  # down-right
        36: (0, 1),  # down
        35: (-1, 1),  # down-left
    }

    def __init__(self, container, events):
        """
        This is the player-controlled character. It's white, of course
        """
        GameCharacter.__init__(self, container, events)
        self.name = 'player'
        self.color = tcod.white
        self.set_attributes(max_hp=20, atk_pwr=2)

    def interaction(self, key):
        """
        Handles PC<->environment interactions, after the player presses a key

        :param key: libtcod key event
        :return: True if the Player moved, False if he did not
        """
        if key.vk in self.directions:
            x, y = self.directions[key.vk]
            self._interact(delta_x=x, delta_y=y)
            return True
        else:
            self._interact_on_top(key)
            return False

    def _interact_on_top(self, key):
        """
        Used to interact with items on the same tile where the player is

        :param key: libtcod key event
        :return:
        """
        self._go_downstairs(key)
        self._go_upstairs(key)
        if key.vk == tcod.KEY_KP5:
            for obj in self.game_objects:
                if (obj.x, obj.y) == (self.x, self.y) and isinstance(obj, Item):
                    obj.pick_up(self.inventory)
                    break

    def _go_upstairs(self, key):
        """
        Moves the player up one level

        :param key: libtcod key event
        :return:
        """
        if chr(key.c) == ".":
            for obj in self.game_objects:
                if (obj.x, obj.y) == (self.x, self.y) and isinstance(obj, UpStairs):
                    print("going upstairs")

    def _go_downstairs(self, key):
        """
        Moves the player down one level

        :param key: libtcod key event
        :return:
        """
        if chr(key.c) == ",":
            for obj in self.game_objects:
                if (obj.x, obj.y) == (self.x, self.y) and isinstance(obj, DownStairs):
                    print("going downstairs")

    def _interact_up(self, key):
        """
        Interactions when going up

        :param key: libtcod key event
        :return:
        """
        if key.vk == tcod.KEY_UP or key.vk == tcod.KEY_KP8:
            print(tcod.KEY_KP8)
            self._interact(delta_x=0, delta_y=-1)
            return True

    def _interact_down(self, key):
        """
        Interactions when going down

        :return:
        """
        if key.vk == tcod.KEY_DOWN or key.vk == tcod.KEY_KP2:
            print(tcod.KEY_KP2)
            self._interact(delta_x=0, delta_y=1)
            return True

    def _interact_left(self, key):
        """
        Interactions when going left

        :return:
        """
        if key.vk == tcod.KEY_LEFT or key.vk == tcod.KEY_KP4:
            print(tcod.KEY_KP4)
            self._interact(delta_x=-1, delta_y=0)
            return True

    def _interact_right(self, key):
        """
        Interactions when going right

        :return:
        """
        if key.vk == tcod.KEY_RIGHT or key.vk == tcod.KEY_KP6:
            print(tcod.KEY_KP6)
            self._interact(delta_x=1, delta_y=0)
            return True

    def _interact_up_left(self, key):
        """
        Interactions when going down left

        :return:
        """
        if key.vk == tcod.KEY_KP7:
            self._interact(delta_x=-1, delta_y=-1)
            return True

    def _interact_down_left(self, key):
        """
        Interactions when going down left

        :return:
        """
        if key.vk == tcod.KEY_KP1:
            self._interact(delta_x=-1, delta_y=1)
            return True

    def _interact_up_right(self, key):
        """
        Interactions when going up right

        :return:
        """
        if key.vk == tcod.KEY_KP9:
            self._interact(delta_x=1, delta_y=-1)
            return True

    def _interact_down_right(self, key):
        """
        Interactions when going up left

        :return:
        """
        if key.vk == tcod.KEY_KP3:
            self._interact(delta_x=1, delta_y=1)
            return True
