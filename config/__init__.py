import os


def read_config(path_to_file, my_dict):
    """
    Read the setup file and save its parameters.
    :param my_dict:
    :param path_to_file: setup file to read from
    :return:
    """
    setup_file = open(os.path.relpath(path_to_file), 'r')
    with setup_file as s_f:
        save_parameters(s_f, my_dict)
    setup_file.close()


def save_parameters(s_f, my_dict):
    """
    Read each line from the setup file and store parameters in the dictionary.
    :param my_dict:
    :param s_f: setup file to read
    :return:
    """
    for line in s_f.read().splitlines():
        param = line.split(":")[0]
        if line.split(":")[1].startswith('0x'):
            param_value = int(line.split(":")[1], 16)
        else:
            try:
                param_value = int(line.split(":")[1])
            except ValueError:
                param_value = str(line.split(":")[1])
        my_dict[param] = param_value


class Colour:
    def __init__(self):
        """
        This class' attributes are different colour presented in hexadecimal values
        """
        self.COLOURS = {}
        self.build_colours_dictionary()
        self.make_colour_attributes(self.COLOURS)

    def build_colours_dictionary(self):
        """
        Reads the palette.txt file and builds a readable dictionary from it.
        :return:
        """
        try:
            read_config("config/palette.txt", self.COLOURS)
        except IOError:
            read_config("palette.txt", self.COLOURS)

    def make_colour_attributes(self, colours_dict):
        """
        Converts the COLOURS dictionary to attributes of the class.
        :param colours_dict:
        :return:
        """
        for colour in colours_dict:
            setattr(self, colour, colours_dict[colour])


class Parameter:
    parameters = {}

    def __init__(self):
        """
        This class contains all the parameters, as defined in the configuration.txt.
        """
        self.build_parameters_dictionary()
        self.make_parameters_attributes(self.parameters)
        self.inv_xy = [(2 + 4 * x, 4 + 3 * y) for y in range(3) for x in range(3)]
        self.eqp_xy = [(2 + 4 * x, 4 + 3 * y) for y in range(3) for x in range(3)]

    def build_parameters_dictionary(self):
        """
        Reads the configuration.txt file and builds a readable dictionary from it.

        :return:
        """
        try:
            read_config("config/config", self.parameters)
        except IOError:
            read_config("config", self.parameters)

    def make_parameters_attributes(self, params_dict):
        """
        Converts parameters collected in the dictionary into class' attributes

        :param params_dict: dictionary with parameters' names (keys) values (values)
        :type params_dict: dict
        :return:
        """
        for param in params_dict:
            setattr(self, param, params_dict[param])
        self._set_window_width(params_dict)
        self._set_window_height(params_dict)
        self._set_level_edge(params_dict)

    def _set_level_edge(self, params_dict):
        if 'level_edge' not in params_dict:
            setattr(self, 'level_edge', max(params_dict['level_width'], params_dict['level_height']))

    def _set_window_height(self, params_dict):
        if 'window_height' not in params_dict:
            total_height = params_dict['info_panel_height'] \
                           + params_dict['gui_tabs_height']
            setattr(self, 'window_height', total_height)

    def _set_window_width(self, params_dict):
        if 'window_width' not in params_dict:
            total_width = params_dict['gui_tabs_width'] * 2 + params_dict['camera_width']
            setattr(self, 'window_width', total_width)


conf = Parameter()
