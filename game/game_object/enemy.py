import tcod
from random2 import randint

from .g_char import GameCharacter
from .item import HealingPotion, Sword


class Enemy(GameCharacter):
    def __init__(self, container, events):
        """
        A generic enemy is hostile to the player

        :return:
        """
        GameCharacter.__init__(self, container, events)
        self.hostile = True


class Goblin(Enemy):
    def __init__(self, container, events):
        """
        a gobbo
        """
        Enemy.__init__(self, container, events)
        self.name = 'goblin'
        self.char = 103
        self.color = tcod.light_green
        self.randomize_spawn()

    def randomize_spawn(self):
        d10 = randint(0, 9)
        if d10 <= 6:
            self.scout()
        elif 6 < d10 <= 8:
            self.sergeant()
        elif d10 == 9:
            self.brute()

    def scout(self):
        self.stats["exp"] = 1
        self.name += ' scout'
        self.color = tcod.lighter_green
        self.tall = False
        self.set_attributes(2, 2)
        HealingPotion().add_to_inventory(self.inventory, self.game_objects)

    def sergeant(self):
        self.stats["exp"] = 3
        self.name += ' sarge'
        self.color = tcod.light_green
        self.tall = False
        self.set_attributes(4, 2)
        HealingPotion().add_to_inventory(self.inventory, self.game_objects)
        Sword().add_to_inventory(self.inventory, self.game_objects)

    def brute(self):
        self.stats["exp"] = 5
        self.name += ' brute'
        self.color = tcod.desaturated_green
        self.tall = True
        self.set_attributes(7, 5)


class Orc(Enemy):
    def __init__(self):
        Enemy.__init__(self)
        self.name = 'orc'
        self.char = 111
        self.color = tcod.darkest_green
        self.set_attributes(5, 3)
