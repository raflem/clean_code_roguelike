import libtcodpy164 as libtcod


class Texter:
    def __init__(self):
        self.timer = 0
        self.command = []
        self.x = 0
        self.width = 80
        libtcod.console_set_custom_font("data/fonts/terminal12x12_gs_ro.png",
                                        libtcod.FONT_TYPE_GREYSCALE | libtcod.FONT_LAYOUT_ASCII_INROW)
        libtcod.console_init_root(self.width, 25, 'Testing input keys', False)
        self.fps = 20
        self.running = True
        libtcod.sys_set_fps(self.fps)

    def text_something(self):

        while not libtcod.console_is_window_closed() and self.running:
            key = libtcod.console_check_for_keypress(libtcod.KEY_PRESSED)
            self.timer += 1
            if self.timer % 5 == 0:
                if self.timer % 3 == 0:
                    self.timer = 0
                    libtcod.console_set_char_background(0, self.x, 0, libtcod.white)
                else:
                    self.clear_tile()

            self.exit(key.vk)
            self.backspacing()
            self.write(key.c)
            self.print_ascii()
            libtcod.console_flush()

    def backspacing(self):
        if libtcod.console_is_key_pressed(libtcod.KEY_BACKSPACE) and self.x > 0:
            self.clear_tile()
            self.command = self.command[:-1]
            self.x -= 1

    def clear_tile(self):
        libtcod.console_set_char(0, self.x, 0, " ")
        libtcod.console_set_char_foreground(0, self.x, 0, libtcod.white)
        libtcod.console_set_char_background(0, self.x, 0, libtcod.black)

    def exit(self, key):
        if key is libtcod.KEY_ENTER or key is libtcod.KEY_ESCAPE:
            return True

    def write(self, key):
        if key > 0:
            letter = chr(key)
            libtcod.console_put_char_ex(0, self.x, 0, letter, libtcod.white, libtcod.black)
            self.command += letter
            self.x += 1

    def print_ascii(self):
        for char in range(0, 256):
            x = 0
            y = 1
            if x < self.width:
                libtcod.console_put_char_ex(0, x, y, chr(char), libtcod.white, libtcod.black)
                x += 1
            else:
                x = 0
                y += 1


Texter().text_something()


def render_hp_as_flowers(self, console, my_x, my_y):
    """
    Renders the player's health in form of 4-petalled flowers
    :param console:
    :param my_x:
    :param my_y:
    :return:
    """
    for i in range(self.stats["max_hp"] // 4):
        for y in range(0, 5):
            for x in range(0, 5):
                self.render_hp_flower(console, i, my_x, my_y, x, y)


def render_hp_flower(self, console, i, my_x, my_y, x, y):
    """
    TODO: this is a mess
    Renders the player's health in form of a 4-petalled flower
    :param console: where to render
    :param i: iterator, allows to render more flowers
    :param my_x: horizontal offset
    :param my_y: vertical offset
    :param x: horizontal position
    :param y: vertical position
    :return:
    """
    hp_flower_center(console, my_x + i * 6, my_y, x, y)
    if self.stats["hp"] != 0:
        self._four_petals(console, i, my_x, my_y, x, y)
        self._three_petals(console, i, my_x, my_y, x, y)
        self.two_petals(console, i, my_x, my_y, x, y)
        self._one_petal(console, i, my_x, my_y, x, y)


def _one_petal(self, console, i, my_x, my_y, x, y):
    if self.stats["hp"] >= 1 + i * 4:
        bot_hp_petal(console, my_x + i * 6, my_y, x, y)
    else:
        bot_hp_petal(console, my_x + i * 6, my_y, x, y, color=libtcod.darkest_red)


def two_petals(self, console, i, my_x, my_y, x, y):
    if self.stats["hp"] >= 2 + i * 4:
        left_hp_petal(console, my_x + i * 6, my_y, x, y)
    else:
        left_hp_petal(console, my_x + i * 6, my_y, x, y, color=libtcod.darkest_red)


def _three_petals(self, console, i, my_x, my_y, x, y):
    if self.stats["hp"] >= 3 + i * 4:
        right_hp_petal(console, my_x + i * 6, my_y, x, y)
    else:
        right_hp_petal(console, my_x + i * 6, my_y, x, y, color=libtcod.darkest_red)


def _four_petals(self, console, i, my_x, my_y, x, y):
    if self.stats["hp"] >= 4 + i * 4:
        top_hp_petal(console, my_x + i * 6, my_y, x, y)
    else:
        top_hp_petal(console, my_x + i * 6, my_y, x, y, color=libtcod.darkest_red)


# TODO: all these four can GTFO to window or something, player shouldn't handle rendering his shit
def left_hp_petal(console, my_x, my_y, x, y, color=libtcod.dark_red):
    if x == 0 and y == 1:
        libtcod.console_put_char_ex(console, x + my_x, y + my_y, 201, color, libtcod.black)  # top left corner
    if x == 0 and y == 2:
        libtcod.console_put_char_ex(console, x + my_x, y + my_y, 186, color, libtcod.black)  # vertical bar
    if x == 0 and y == 3:
        libtcod.console_put_char_ex(console, x + my_x, y + my_y, 200, color, libtcod.black)  # bot left corner
    if y == 3 and (x == 1 or x == 2):
        libtcod.console_put_char_ex(console, x + my_x, y + my_y, 205, color, libtcod.black)  # horizontal bar


def right_hp_petal(console, my_x, my_y, x, y, color=libtcod.dark_red):
    if x == 4 and y == 3:
        libtcod.console_put_char_ex(console, x + my_x, y + my_y, 188, color, libtcod.black)  # bot right corner
    if x == 4 and y == 2:
        libtcod.console_put_char_ex(console, x + my_x, y + my_y, 186, color, libtcod.black)  # vertical bar
    if x == 4 and y == 1:
        libtcod.console_put_char_ex(console, x + my_x, y + my_y, 187, color, libtcod.black)  # top right corner
    if y == 1 and (x == 2 or x == 3):
        libtcod.console_put_char_ex(console, x + my_x, y + my_y, 205, color, libtcod.black)  # horizontal bar


def bot_hp_petal(console, my_x, my_y, x, y, color=libtcod.dark_red):
    if x == 1 and y == 4:
        libtcod.console_put_char_ex(console, x + my_x, y + my_y, 200, color, libtcod.black)  # bot left corner
    if x == 2 and y == 4:
        libtcod.console_put_char_ex(console, x + my_x, y + my_y, 205, color, libtcod.black)  # horizontal bar
    if x == 3 and y == 4:
        libtcod.console_put_char_ex(console, x + my_x, y + my_y, 188, color, libtcod.black)  # bot right corner
    if x == 3 and (y == 2 or y == 3):
        libtcod.console_put_char_ex(console, x + my_x, y + my_y, 186, color, libtcod.black)  # vertical bar


def top_hp_petal(console, my_x, my_y, x, y, color=libtcod.dark_red):
    if x == 1 and y == 0:
        libtcod.console_put_char_ex(console, x + my_x, y + my_y, 201, color, libtcod.black)  # top left corner
    if x == 2 and y == 0:
        libtcod.console_put_char_ex(console, x + my_x, y + my_y, 205, color, libtcod.black)  # vertical bar
    if x == 3 and y == 0:
        libtcod.console_put_char_ex(console, x + my_x, y + my_y, 187, color, libtcod.black)  # top right corner
    if x == 1 and (y == 1 or y == 2):
        libtcod.console_put_char_ex(console, x + my_x, y + my_y, 186, color, libtcod.black)  # horizontal bar


def hp_flower_center(console, my_x, my_y, x, y, color=libtcod.darker_red):
    if x == 2 and y == 2:
        libtcod.console_put_char_ex(console, x + my_x, y + my_y, 'o', color, libtcod.black)  # center
