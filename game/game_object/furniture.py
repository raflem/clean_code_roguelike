import tcod

from .item import Item
from .g_obj import GameObject


class Furniture(GameObject):
    """
    Furniture can be interacted with, but usually doesn't interact back;

    Cannot be picked up; i.e. doors, chests, stools, tables
    """
    def __init__(self):
        GameObject.__init__(self)
        self.interact = True
        self.stats = {
            'max_hp': 1,
            'hp': 1,
        }

    def assemble(self, position, level, container):
        """
        Spawns the character, assigning it a place on the level, adding it to the container,
        and blocking sight of the tile, if needed.

        :type position: tuple
        :type level: list
        :param level: level on which the character is represented
        :param container: collection of all game objects
        """
        self.x, self.y = position
        self.game_objects = container
        self.game_objects.append(self)
        self.level = level

    def wreck(self):
        """
        Destroys the furniture, leaving some wreck on the floor
        """
        if self.stats['hp'] <= 0:
            self._unblock_own_tile()
            self.game_objects.remove(self)
            wreck = Item('wrecked {}'.format(self.name), 44, self.color)
            wreck.spawn((self.x, self.y), self.level, self.game_objects)


class Door(Furniture):
    """
    A simple door
    """

    def __init__(self):
        Furniture.__init__(self,)
        self.name = 'door'
        self.char = '+'
        self.color = tcod.darker_flame
        self.tall = True
        self.open = False
        self._status_updater()

    def ajar(self):
        """
        Opens the door ajar;
        Cannot be private, because GameCharacter uses it
        Cannot be named 'open,' because that's too broad/generic
        """
        self.open = True
        self._unblock_own_tile()
        self.char = 39
        self._status_updater()

    def _status_updater(self):
        """
        Updates door name depending on state
        """
        if self.open:
            self.name = self.name.split('(')[0] + '(open)'
        else:
            self.name = self.name.split('(')[0] + '(closed)'


class UpStairs(Furniture):
    """
    Stairs leading up
    """

    def __init__(self):
        Furniture.__init__(self)
        self.name = 'stairs(up)'
        self.color = tcod.white
        self.char = 30


class DownStairs(UpStairs):
    """
    Stairs leading down.
    """

    def __init__(self):
        UpStairs.__init__(self)
        self.name = 'stairs(down)'
        self.char = 31


class Container(Furniture):
    """
    They contain items
    """

    def __init__(self):
        Furniture.__init__(self)
        # self._block_own_tile()
        self.open = False
        self.contents = []

    def pop(self):
        self.open = True

    def _status_updater(self):
        """
        Updates Container name depending on state
        """
        if self.open:
            self.name = self.name.split('(')[0] + '(open)'
        else:
            self.name = self.name.split('(')[0] + '(closed)'


class Chest(Container):
    def __init__(self):
        Container.__init__(self)
        self.name = 'chest'
        self.char = 127
        self.color = tcod.darker_sepia


class Shelf(Container):
    def __init__(self):
        Container.__init__(self)
        self.name = 'shelf'
        self.char = 240
        self.color = tcod.darker_sepia
