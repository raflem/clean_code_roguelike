import textwrap
from math import sqrt

import tcod

from .furniture import Door, Container
from .item import Corpse
from .g_obj import GameObject


class GameCharacter(GameObject):
    """
    Game Characters are actors in the game, they can be fought or talked to.
    """

    def __init__(self, container, events):
        GameObject.__init__(self, 'npc', 64, tcod.yellow)
        self.game_objects = container
        self.events = events
        self.game_objects.append(self)
        self.stats = {
            "max_hp": 1,
            "hp": 1,
            "atk_pwr": 1,
            "speed": 1,
            "sight": 1,
            "smell": 1,
            "sneak": 1,
            "exp": 0,
        }
        self.equipment = {
            0: None,  # trinket
            1: None,  # helmet
            2: None,  # necklace
            3: None,  # weapon
            4: None,  # armor
            5: None,  # offhand
            6: None,  # glove
            7: None,  # boots
            8: None,  # ring
        }
        self.inventory = {
            0: None,
            1: None,
            2: None,
            3: None,
            4: None,
            5: None,
            6: None,
            7: None,
            8: None,
        }
        self.tall = True
        self.set_attributes()

    def spawn(self, position, level):
        """
        Spawns the character, assigning it a place on the level, adding it to the container,
        and blocking sight of the tile, if needed.

        :type position: tuple
        :type level: list
        :param level: level on which the character is represented
        """
        if level[position[0]][position[1]].walkable:
            self.x, self.y = position
            self.level = level

    def set_attributes(self, max_hp=2, atk_pwr=1):
        """
        Sets the character's attributes when spawning

        :type max_hp: int
        :type atk_pwr: int
        """
        self.stats["max_hp"] = max_hp
        self.stats["hp"] = self.stats["max_hp"]
        self.stats["atk_pwr"] = atk_pwr

    def _interact(self, delta_x, delta_y):
        """
        Gathers possible interactions

        :type delta_x: int
        :type delta_y: int
        """
        if not self._use_furniture(delta_x, delta_y):
            if not self._attack(delta_x, delta_y):
                self._move(delta_x, delta_y)

    def _use_furniture(self, delta_x, delta_y):
        """
        Opens doors and chests

        :return:
        """
        if self._open_doors(delta_x, delta_y) or self._pop_containers():
            self.event("{} opens the door.".format(self.name).capitalize())
            return True

    def _pop_containers(self):
        """
        Used to open containers, like chests

        :return:
        """
        for cont in self.game_objects:
            neighbour_x = sqrt((cont.x - self.x) ** 2) <= 1
            neighbour_y = sqrt((cont.y - self.y) ** 2) <= 1
            if isinstance(cont, Container) and neighbour_x and neighbour_y:
                cont.pop()
                return True

    def _open_doors(self, delta_x, delta_y):
        """
        Opens nearby doors.

        :return:
        """
        for door in self.game_objects:
            if isinstance(door, Door) and (door.x, door.y) == (self.x + delta_x, self.y + delta_y) and not door.open:
                door.ajar()
                return True

    def _attack(self, dx, dy):
        """
        Basic attack, when one GameCharacter tries to move on top of another.

        :param dx: delta x
        :param dy: delta y
        """
        x, y = self.x + dx, self.y + dy
        for obj in self.game_objects:
            if self._deal_damage(obj, x, y):
                return True

    def _deal_damage(self, obj, x, y):
        if isinstance(obj, GameCharacter) and not isinstance(obj, self.__class__) and (obj.x, obj.y) == (x, y):
            obj.stats["hp"] -= self.stats["atk_pwr"]
            self.event("{} hit {} for {} HP.".format(self.name, obj.name, self.stats["atk_pwr"]).capitalize())
            self._kill_exp_delete(obj)
            return True

    def _kill_exp_delete(self, obj):
        if obj.die():
            self.event("{} dies.".format(obj.name).capitalize(), tcod.dark_red)
            self.stats["exp"] += obj.stats["exp"]
            del obj

    def follow_player(self, player):
        """
        Simple algorithm that will follow the player, also diagonally.

        :param player: the player character to follow
        """
        diff_x, diff_y = player.x - self.x, player.y - self.y
        distance = sqrt(diff_x ** 2 + diff_y ** 2)
        if distance != 0:
            delta_x, delta_y = int(round(diff_x / distance)), int(
                round(diff_y / distance))  # round() used to get integer
        else:
            delta_x, delta_y = 0, 0
        self._interact(delta_x, delta_y)

    def die(self):
        """
        Kills the character

        :return:
        """
        if self.stats["hp"] <= 0:
            self.game_objects.remove(self)
            self._unblock_own_tile()
            self._dequip_equipment()
            self._drop_items()
            Corpse(self.name).spawn((self.x, self.y), self.level, self.game_objects)
            return True

    def _dequip_equipment(self):
        for eqp in self.equipment.values():
            if eqp:
                eqp.dequip(self)

    def _drop_items(self):
        for _item in self.inventory.values():
            if _item:
                _item.drop(self)

    def event(self, event_text, color=tcod.white):
        event_lines = textwrap.wrap(event_text, 11)
        for event_line in event_lines:
            if len(self.events) == 15:
                del self.events[0]
            self.events.append((event_line, color))
