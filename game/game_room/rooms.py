from math import sqrt

from ..game_object.furniture import UpStairs, DownStairs, Door
from ..game_map.wall import StoneWall, Pillar
from ..game_map.floor import StoneFloor


class Room:
    def __init__(self, x1, y1):
        """
        Every generic room in the game.

        :param x1:
        :param y1:
        """
        self.x1 = x1
        self.y1 = y1
        self.x2 = 0
        self.y2 = 0
        self.center_x, self.center_y = 0, 0

        self.level = None
        self.console = None
        self.container = None

    def set_params(self, console, level, container):
        """
        Used to set the console and level tileset, used by the GameMap

        :param console:
        :param container:
        :param level:
        """
        self.console = console
        self.level = level
        self.container = container

    def center(self):
        """
        Find the center tile of the room.

        :return: x and y of the center
        """
        center_x = int((self.x1 + self.x2) / 2)
        center_y = int((self.y1 + self.y2) / 2)
        return center_x, center_y

    def make_vertical_wall(self, start_y, length, constant_x, wall_type=StoneWall):
        """
        Creates a single-line-thick vertical wall; min() and max() enable drawing walls 'backwards'

        :type start_y: int
        :type length: int
        :type constant_x: int
        :type wall_type: Wall
        :return:
        """
        end_y = start_y + length
        for y in range(min(start_y, end_y), max(start_y, end_y)):
            try:
                self.level[constant_x][y] = wall_type(self.console)
            except IndexError:
                continue

    def make_horizontal_wall(self, start_x, length, constant_y, wall_type=StoneWall):
        """
        Creates a single-line-thick horizontal wall; min() and max() enable drawing walls 'backwards'

        :type start_x: int
        :type length: int
        :type constant_y: int
        :type wall_type: Wall
        """
        end_x = start_x + length
        for x in range(min(start_x, end_x), max(start_x, end_x)):
            try:
                self.level[x][constant_y] = wall_type(self.console)
            except IndexError:
                continue


class Circle(Room):
    def __init__(self, diameter):
        """
        A circle-shaped room. It behaves like a Square room, but has less surface area.

        :type diameter: int
        :param diameter: the diameter of the room
        """
        Room.__init__(self, x1=0, y1=0)
        self.diameter = diameter
        self.center_x = 0
        self.center_y = 0

    def set_boundaries(self, x1, y1):
        """
        Sets the top-left corner of the room and then the bottom right corner and the center.

        :param x1: new top-left x coordinate
        :param y1: new top-left y coordinate
        """
        self.x1, self.y1 = x1, y1
        self.x2 = self.x1 + self.diameter
        self.y2 = self.y1 + self.diameter
        self.center_x, self.center_y = self.center()

    def _make_doors(self, *sides):
        """
        Creates openings/doors in the walls

        :type side: str
        """
        if 'top' in sides:
            x, y = self.center_x, self.y1
            Door().spawn((x, y), level=self.level, container=self.container)
        if 'bot' in sides:
            x, y = self.center_x, self.y2
            Door().spawn((x, y), level=self.level, container=self.container)
        if 'left' in sides:
            x, y = self.x1, self.center_y
            Door().spawn((x, y), level=self.level, container=self.container)
        if 'right' in sides:
            x, y = self.x2, self.center_y
            Door().spawn((x, y), level=self.level, container=self.container)

    def dig_out(self, start_x, start_y):
        """
        Creates Floor tiles on the entirety of the circle

        :type start_x: int
        :type start_y: int
        """
        self.set_boundaries(start_x, start_y)
        for x in range(self.x1, self.x2):
            for y in range(self.y1, self.y2):
                if sqrt((x - self.center_x)**2 + (y - self.center_y)**2) < self.diameter/2:
                    self.level[x][y] = StoneFloor(self.console)
        self._make_doors(*['t', 'b', 'l', 'r'])

    def build_boundary(self, start_x, start_y):
        """
        Creates a wall around the circle.

        :type start_x: int
        :type start_y: int
        """
        self.set_boundaries(start_x, start_y)
        radius = self.diameter / 2
        for x in range(self.x1, self.x2):
            for y in range(self.y1, self.y2):
                if radius - 1 <= sqrt((x - self.center_x)**2 + (y - self.center_y)**2) <= radius:
                    self.level[x][y] = StoneWall(self.console)
        self._make_doors('b')


class Rectangle(Room):
    def __init__(self, width, height):
        """
        A simple rectangular room

        :type width: int
        :type height: int
        """
        Room.__init__(self, x1=0, y1=0)
        self.width = width
        self.height = height

    def set_corners(self, x1, y1):
        """
        Sets the top-left corner of the room and then the bottom right corner.

        :param x1: new top-left x coordinate
        :param y1: new top-left y coordinate
        """
        self.x1, self.y1 = x1, y1
        self.x2 = self.x1 + self.width
        self.y2 = self.y1 + self.height
        self.center_x, self.center_y = self.center()

    def intersects_other(self, other_rectangle):
        """
        Determines if this rectangle directly intersects with another one

        :param other_rectangle: rectangle to check against
        :return: logical True if two rectangles are overlapping
        """
        return (self.x1 <= other_rectangle.x2 and self.y1 <= other_rectangle.y2
                and self.x2 >= other_rectangle.x1 and self.y2 >= other_rectangle.y1)

    def _top_wall_center(self):
        """
        Find the center tile of the topmost wall

        :return: x and y of the top wall center
        """
        top_wall_center_x = int((self.x1 + self.x2) / 2)
        top_wall_center_y = self.y1 - 1
        return top_wall_center_x, top_wall_center_y

    def _bottom_wall_center(self):
        """
        Find the center tile of the bottommost wall

        :return: x and y of the bottom wall center
        """
        bottom_wall_center_x = int((self.x1 + self.x2) / 2)
        bottom_wall_center_y = self.y2
        return bottom_wall_center_x, bottom_wall_center_y

    def _left_wall_center(self):
        """
        Find the center tile of the leftmost wall

        :return: x and y of the left wall center
        """
        left_wall_center_x = self.x1 - 1
        left_wall_center_y = int((self.y1 + self.y2) / 2)
        return left_wall_center_x, left_wall_center_y

    def _right_wall_center(self):
        """
        Find the center tile of the rightmost wall

        :return: x and y of the right wall center
        """
        right_wall_center_x = self.x2
        right_wall_center_y = int((self.y1 + self.y2) / 2)
        return right_wall_center_x, right_wall_center_y

    def _build_vertical_walls(self):
        """
        Builds two walls along the y axis (x is constant)

        :return:
        """
        self.make_vertical_wall(self.y1, self.height, self.x1)
        self.make_vertical_wall(self.y1, self.height, self.x2)

    def _build_horizontal_walls(self):
        """
        Builds two walls along the x axis (y is constant)

        :return:
        """
        self.make_horizontal_wall(self.x1, self.width, self.y1)
        self.make_horizontal_wall(self.x1, self.width, self.y2)

    def build(self, start_x, start_y):
        """
        Creates four walls and adds doors in a level of Floor

        :param start_x: top left x coordinate of the wall
        :param start_y: top left y coordinate of the wall
        """
        self.set_corners(start_x, start_y)
        self._build_vertical_walls()
        self._build_horizontal_walls()
        self.make_doors('top')
        self.make_doors('bot')
        self.make_doors('left')
        self.make_doors('right')

    def dig_out(self, start_x, start_y):
        """
        Digs the room out of a level full of Wall

        :param start_x:
        :param start_y:
        """
        self.set_corners(start_x, start_y)
        for x in range(self.x1, self.x2):
            for y in range(self.y1, self.y2):
                try:
                    self.level[x][y] = StoneFloor(self.console)
                except IndexError:
                    print("Room with xy=(%s, %s) went out of bounds!" % (start_x, start_y))
                    continue

    def make_doors(self, side):
        """
        Creates openings/doors in the walls

        :type side: str
        """
        x, y = 0, 0
        if side == 'top' or side.startswith('t'):
            x, y = self._top_wall_center()
        elif side == 'bot' or side.startswith('b'):
            x, y = self._bottom_wall_center()
        elif side == 'left' or side.startswith('l'):
            x, y = self._left_wall_center()
        elif side == 'right' or side.startswith('r'):
            x, y = self._right_wall_center()
        Door().assemble((x, y), level=self.level, container=self.container)


class Square(Rectangle):
    def __init__(self, edge):
        """
        Square room

        :type edge: int
        :param edge: length of the edge
        """
        Rectangle.__init__(self, width=edge, height=edge)

    def build_central_pillars(self):
        """
        Creates 4 pillars around the middle

        :return:
        """
        for x in range(-1, 2, 2):
            for y in range(-1, 2, 2):
                self.level[self.center_x + x][self.center_y + y] = Pillar(self.console)

    def build_central_stairs(self):
        """
        Creates a pair of stairs in the middle of the room.

        :return:
        """
        upstairs = UpStairs()
        upstairs.assemble((self.center_x + 1, self.center_y), self.level, self.container)
        downstairs = DownStairs()
        downstairs.assemble((self.center_x - 1, self.center_y), self.level, self.container)

    def build_central_hub(self):
        """
        Creates a wall structure, surrounding the stairs.

        :return:
        """
        for x in range(self.center_x - 2, self.center_x + 3):
            for y in range(self.center_y - 1, self.center_y + 2):
                self.level[x][y] = StoneWall(self.console)
        self.level[self.center_x][self.center_y] = StoneFloor(self.console)
        for x in range(self.center_x - 1, self.center_x + 2):
            for y in range(self.center_y - 1, self.center_y + 2):
                if x**2 != y**2:
                    self.level[x][y] = StoneFloor(self.console)


