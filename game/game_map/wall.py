import tcod
from .tile import Tile


class Wall(Tile):
    """
    Default wall tile, inherits the default colour

    :return:
    """
    name = 'wall'
    char = 35
    transparent = False
    walkable = False

    def __init__(self, console):
        Tile.__init__(self, console=console)
        self.foreground_color = tcod.white


class StoneWall(Wall):
    """
    Stone wall is light gray
    """
    name = 'stone wall'

    def __init__(self, console):
        """

        :param console:
        """
        Wall.__init__(self, console=console)
        self.foreground_color = tcod.gray


class Pillar(Wall):
    char = 73
    name = 'pillar'

    def __init__(self, console):
        """

        :param console: libtcod console onto which the tile is blit
        """
        Wall.__init__(self, console=console)
        self.foreground_color = tcod.gray


class VineWall(Wall):
    name = 'vine wall'

    def __init__(self, console):
        """

        :param console: libtcod console onto which the tile is blit
        """
        Wall.__init__(self, console=console)
        self.foreground_color = tcod.darker_green
        self.background_color = tcod.desaturated_green
