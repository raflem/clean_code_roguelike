from gui_tab import *


class SpellBook(GuiTab):
    def __init__(self, player, mouse):
        GuiTab.__init__(self, player, mouse)
        self.console = console_new(conf.gui_tabs_width, conf.gui_tabs_height)
        self.offset_x = conf.gui_tabs_width + conf.camera_width
        self.offset_y = 0
        self.corners = {
            (self.offset_x, self.offset_y): 201,
            (self.offset_x, conf.gui_tabs_height - 1): 200,
            (self.offset_x + conf.gui_tabs_width - 1, self.offset_y + conf.gui_tabs_height - 1): 188,
            (self.offset_x + conf.gui_tabs_width - 1, self.offset_y): 187,
            (self.offset_x + conf.gui_tabs_width - 5, self.offset_y): 200,
            (self.offset_x + conf.gui_tabs_width - 9, self.offset_y): 188,
        }
        self.blit_params = {
            'src': self.console,
            'x': 0,
            'y': 0,
            'w': 0,
            'h': 0,
            'dst': 0,
            'xdst': self.offset_x,
            'ydst': 0,
            'ffade': 1.0,
            'bfade': 0.3,
        }
        self.border_colour = purple
        self.tag = "spb"
        self.range = range(self.offset_x, self.offset_x + 5)

    def render(self):
        self.blit(self.range, self.tag)
