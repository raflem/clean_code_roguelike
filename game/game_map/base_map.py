from config import conf
from .wall import StoneWall


class GameMap:
    """
    The actual game map, containing the tiles matrix
    """
    def __init__(self, console):
        """

        :param console:
        """
        self.console = console
        self.level = [
            [StoneWall(console=self.console)
             for width in range(conf.level_width)]
            for height in range(conf.level_height)
        ]
