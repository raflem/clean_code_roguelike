from config import conf


class Camera:
    def __init__(self):
        """
        Camera is the square of tiles that shows the rendered portion of the level.
        x, y are the top-left tile of the rendered square.
        """
        self.x = 0
        self.y = 0

    def adjust_camera(self, target_x, target_y):
        """
        Adjusts the camera coordinates, which are then used to properly render tiles and objects on the screen.

        :type target_x: int
        :type target_y: int
        """
        self.adjust_camera_x(target_x)
        self.adjust_camera_y(target_y)

    def adjust_camera_x(self, target_x):
        """
        Adjusts the camera_x coordinate, making sure it won't go out of bounds.

        :type target_x: int
        """
        new_x = target_x - conf.camera_width / 2
        if new_x < 0:
            new_x = 0
        elif new_x > conf.level_width - conf.camera_width:
            new_x = conf.level_width - conf.camera_width
        self.x = new_x

    def adjust_camera_y(self, target_y):
        """
        Adjusts the camera_y coordinate, making sure it won't go out of bounds.
        """
        new_y = target_y - conf.camera_height / 2
        if new_y > conf.level_height - conf.camera_height:
            new_y = conf.level_height - conf.camera_height
        elif new_y < 0:
            new_y = 0
        self.y = new_y

