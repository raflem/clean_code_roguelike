import tcod

from config import conf


class GameObject:
    game_objects = None
    level = None
    name = ''
    character = ''
    color = tcod.white

    def __init__(self, name='', character='', color=tcod.white):
        """
        A generic game object.

        :type name: str
        :type character: str or int
        :type color: Color
        :param name: object's given name
        :param character: representation of the object on the screen
        :param color: colour of the character, using libtcod colour palette
        """
        self.name = name
        self.char = character
        self.color = color

        self.x = 0
        self.y = 0
        self.tall = False

    def adjust_coordinates(self, camera):
        """
        Adjusts the coordinates of the object, in accordance to its position relative to the camera

        :param camera: camera object, to get coordinates from
        :return:
        """
        x, y = self.x - camera.x, self.y - camera.y
        if conf.camera_width <= x < 0 or conf.camera_height <= y < 0:
            return None, None
        return x, y

    def draw(self, console, camera):
        """
        Puts the character on the console

        :param console: console on which to draw the character
        :param camera: camera object, to get coordinates from
        :return:
        """
        x, y = self.adjust_coordinates(camera)
        if x is not None:
            tcod.console_put_char_ex(console, x, y, self.char, self.color, tcod.black)

    def clear(self, console, camera):
        """
        Removes the character from the screen.

        :param console: console from which to remove the character
        :param camera: camera object to get coordinates from
        """
        x, y = self.adjust_coordinates(camera)
        if x is not None:
            tcod.console_put_char(console, x, y, ' ', tcod.BKGND_NONE)

    def target_tile_blocked(self, dx, dy):
        """
        Determine if the tile you want to move to is blocked.

        :param dx:
        :param dy:
        :return: status of target tile
        """
        try:
            x_dx, y_dy = self.x + dx, self.y + dy
            if not self.level[x_dx][y_dy].walkable:
                return True
            return False
        except IndexError:
            return True

    def _move(self, dx, dy):
        """
        Allows the object to move, unless the target tile is blocked or recovering from the previous action

        :param dx:
        :param dy:
        """
        if not self.target_tile_blocked(dx, dy):
            self._unblock_own_tile()
            self.x += dx
            self.y += dy
            self._block_own_tile()

    def _move_up(self):
        """
        Moves the object one tile up
        """
        self._move(dx=0, dy=-1)

    def _move_down(self):
        """
        Moves the object one tile down
        """
        self._move(dx=0, dy=1)

    def _move_left(self):
        """
        Moves the object one tile left
        :return:
        """
        self._move(dx=-1, dy=0)

    def _move_right(self):
        """
        Moves the object one tile right
        :return:
        """
        self._move(dx=1, dy=0)

    def _unblock_own_tile(self):
        """
        Unblocks the tile under the object

        :return:
        """
        self.level[self.x][self.y].transparent = True
        self.level[self.x][self.y].walkable = True

    def _block_own_tile(self):
        """
        Blocks the tile under the object

        :return:
        """
        self.level[self.x][self.y].walkable = False
        if self.tall:
            self.level[self.x][self.y].transparent = False
        else:
            self.level[self.x][self.y].transparent = True
