from gui_tab import *


class InventoryGui(GuiTab):
    """
    Inventory sheet, containing information on items inside the player's inventory.
    """
    def __init__(self, player, mouse):
        GuiTab.__init__(self, player, mouse)
        self.corners = {
            (self.offset_x, self.offset_y): 201,
            (self.offset_x, conf.gui_tabs_height - 1): 200,
            (self.offset_x + conf.gui_tabs_width - 1, conf.gui_tabs_height - 1): 188,
            (self.offset_x + conf.gui_tabs_width - 1, self.offset_y): 187,
            (self.offset_x + conf.gui_tabs_width - 5, self.offset_y): 200,
            (self.offset_x + conf.gui_tabs_width - 9, self.offset_y): 188,
        }
        self.border_colour = red
        self.tag = "inv"
        self.range = range(self.offset_x + 4, self.offset_x + 9)

    def __highlight_item(self, mouseunder_item):
        """
        Highlights the background of an item

        :param mouseunder_item:
        """
        adjusted_mouse_xy = (self.mouse.cx - self.offset_x, self.mouse.cy - self.offset_y)
        item_xy = (mouseunder_item.inv_x, mouseunder_item.inv_y)
        bkgnd_params = {
            'con': self.console,
            'x': mouseunder_item.inv_x + self.offset_x,
            'y': mouseunder_item.inv_y + self.offset_y,
            'col': lighter_green,
        }
        if adjusted_mouse_xy == item_xy:
            console_set_char_background(**bkgnd_params)
            self._item_interaction(mouseunder_item)

    def _item_interaction(self, clicked_item):
        """
        Right click will use the item, left click will drop it

        :param clicked_item:
        :return:
        """
        if self.mouse.rbutton_pressed:
            if isinstance(clicked_item, item.Equipment):
                clicked_item.equip(self.player)
            else:
                clicked_item.use(self.player)
        elif self.mouse.lbutton_pressed:
            clicked_item.drop(self.player)

    def __draw_item(self, carried_item):
        """
        Draws the existing item's character in inventory

        :param carried_item:
        :type carried_item: item.Item
        """
        if carried_item:
            item_params = {
                'con': self.console,
                'x': carried_item.inv_x + self.offset_x,
                'y': carried_item.inv_y + self.offset_y ,
                'c': carried_item.char,
                'fore': carried_item.color,
                'back': self.background,
            }
            console_put_char_ex(**item_params)
            self.__highlight_item(carried_item)

    def _render_inventory_items(self):
        """
        Renders the items inside the player's inventory.
        """
        for _item in self.player.inventory.values():
            self.__draw_item(_item)

    def _render_item_name(self):
        """
        Shows the name of the item on the bottom of the inventory tab.

        :return:
        """
        _item_name = self.get_item_name_under_mouse()
        _item_info_params = {
            'con': self.console,
            'x': 1,
            'y': conf.gui_tabs_height - 4,
            'w': conf.gui_tabs_width - 2,
            'h': 3,
            'flag': BKGND_NONE,
            'alignment': LEFT,
            'fmt': _item_name
        }
        console_print_rect_ex(**_item_info_params)

    def _render_inventory_slots(self):
        """
        Renders the slots/holders for the inventory items.

        :return:
        """
        for coord in conf.inv_xy:
            new_x, new_y = coord
            item_holder_params = {
                'con': self.console,
                'x': new_x + self.offset_x,
                'y': new_y + self.offset_y,
                'flag': BKGND_NONE,
                'alignment': CENTER,
                'fmt': '[ ]',
            }
            item_params = {
                'con': self.console,
                'x': new_x + self.offset_x,
                'y': new_y + self.offset_y + 1,
                'flag': BKGND_NONE,
                'alignment': CENTER,
                'fmt': str(conf.inv_xy.index(coord) + 1),
            }
            console_print_ex(**item_holder_params)
            console_print_ex(**item_params)

    def get_item_name_under_mouse(self):
        """
        Creates a single-item list with the name of the item in inventory that's being highlighted

        :return: capitalized name of the item
        :rtype: str
        """
        _item_names = [
            _item.name for _item in self.player.inventory.values()
            if _item and (_item.inv_x, _item.inv_y) == (self.mouse.cx - self.offset_x, self.mouse.cy - self.offset_y)
        ]
        _name = ''.join(_item_names).capitalize()
        return _name

    def render(self):
        """
        Blits the inventory panel to the root console.

        :return:
        """
        self._render_inventory_slots()
        self._render_inventory_items()
        self._render_item_name()
        self.blit(self.range, self.tag)
