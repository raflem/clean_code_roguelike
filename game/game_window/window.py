import tcod

from config import conf
from .camera import Camera
from .character_tab import CharacterGui
from .equipment_tab import EquipmentGui
from .event_log import EventLog
from .inventory_tab import InventoryGui
from game_map import GameMap
from game_object import GameCharacter


# noinspection PyTypeChecker
class GameWindow:
    """
    Everything related to the game's window and rendering.
    """
    console_level = tcod.console_new(conf.camera_width, conf.camera_height)
    console_objects = tcod.console_new(conf.camera_width, conf.camera_height)
    console_fog = tcod.console_new(conf.camera_width, conf.camera_height)
    console_animations = tcod.console_new(conf.camera_width, conf.camera_height)
    console_top_bar = tcod.console_new(conf.window_width, conf.window_height)
    console_left_ui = tcod.console_new(conf.gui_tabs_width, conf.gui_tabs_height)
    console_bottom_ui = tcod.console_new(conf.window_width, conf.info_panel_height)

    fov_map = tcod.map_new(conf.level_width, conf.level_height)
    fov_algorithm = tcod.FOV_SHADOW
    fov_light_walls = True

    cam = Camera()
    off_x = conf.gui_tabs_width
    off_y = conf.top_bar_height
    tcod.console_set_custom_font(str(conf.font), tcod.FONT_TYPE_GREYSCALE | tcod.FONT_LAYOUT_ASCII_INROW)
    tcod.console_init_root(conf.window_width, conf.window_height, f"{conf.title}, ver.: {conf.version}")
    tcod.console_set_default_foreground(0, tcod.dark_yellow)
    tcod.sys_set_fps(20)
    tcod.console_print_ex(0, conf.window_width / 2, conf.window_height / 2, tcod.BKGND_NONE, tcod.CENTER, "Launching...")
    tcod.console_flush()

    def __init__(self, game_objects, mouse, events):
        tcod.console_clear(0)
        tcod.console_set_key_color(self.console_fog, tcod.red)  # red tiles inside fov won't be blitted
        self.timer = 0
        self.elapsed = 0  # seconds since the program started
        self.game_objects = game_objects
        self.player = self.__find_player()
        self.level = GameMap(self.console_level).level
        self.mouse = mouse

        self.inventory_ui = InventoryGui(player=self.player, mouse=self.mouse)
        self.equipment_ui = EquipmentGui(player=self.player, mouse=self.mouse)
        self.character_ui = CharacterGui(player=self.player, mouse=self.mouse)
        self.event_log = EventLog(events)
        self.bookmarks_buttons_xy = {
            'cha': [(x, 1) for x in range(1, 4)],
            'inv': [(x, 1) for x in range(5, 8)],
            'eqp': [(x, 1) for x in range(9, 12)],
        }
        self.active_sheet_left = 'inv'

    def __find_player(self):
        """
        Retrieves the player from the game objects.
        Avoids initialising the player both in the main loop and in here.

        :return:
        """
        for gobj in self.game_objects:
            if gobj.name == 'player':
                return self.game_objects[self.game_objects.index(gobj)]

    def render_bookmarks(self):
        """
        Renders all the bookmarks in hte top left corner of the screen.

        :return:
        """
        titles = {
            "Cha": tcod.green,
            "Eqp": tcod.yellow,
            "Inv": tcod.red,
        }
        _off_x = 0
        self.event_log.render()
        for tag in titles:
            self._render_bookmark(tag, _off_x, titles[tag])
            _off_x += 4
        if self.active_sheet_left == 'eqp':
            self.equipment_ui.render()
        elif self.active_sheet_left == 'inv':
            self.inventory_ui.render()
        elif self.active_sheet_left == 'cha':
            self.character_ui.render()

    def _render_bookmark(self, title, offset_x, color):
        """
        Renders the bookmark, active or not.

        :param title:
        :param offset_x:
        :param color:
        :return:
        """
        blit_params = {
            'src': self.console_top_bar,
            'x': 0,
            'y': 0,
            'w': conf.top_bar_width,
            'h': conf.top_bar_height,
            'dst': 0,
            'xdst': 0,
            'ydst': 0,
            'ffade': 1.0,
            'bfade': 1.0,
        }
        tcod.console_set_default_foreground(self.console_top_bar, tcod.white)
        tcod.console_set_default_background(self.console_top_bar, tcod.black)
        self._render_borders(offset_x, color)
        tcod.console_print_ex(self.console_top_bar, offset_x + 1, 1, tcod.BKGND_NONE, tcod.LEFT, title)
        tcod.console_blit(**blit_params)
        self._switch_to_inventory_tab()
        self._switch_to_equipment_tab()
        self._switch_to_character_tab()

    def _switch_to_character_tab(self):
        if (self.mouse.cx, self.mouse.cy) in self.bookmarks_buttons_xy['cha']:
            self._highlight_bookmark(self.bookmarks_buttons_xy['cha'], tcod.lighter_green)
            if self.mouse.lbutton_pressed:
                self.active_sheet_left = 'cha'

    def _switch_to_equipment_tab(self):
        """
        Provides interaction with the equipment tab.

        :return:
        """
        if (self.mouse.cx, self.mouse.cy) in self.bookmarks_buttons_xy['eqp']:
            self._highlight_bookmark(self.bookmarks_buttons_xy['eqp'], tcod.lighter_yellow)
            if self.mouse.lbutton_pressed:
                self.active_sheet_left = 'eqp'

    def _switch_to_inventory_tab(self):
        """
        Provides interaction with the inventory tab.

        :return:
        """
        if (self.mouse.cx, self.mouse.cy) in self.bookmarks_buttons_xy['inv']:
            self._highlight_bookmark(self.bookmarks_buttons_xy['inv'], tcod.lighter_red)
            if self.mouse.lbutton_pressed:
                self.active_sheet_left = 'inv'

    @staticmethod
    def _highlight_bookmark(buttons_xy, color):
        """
        Highlights the bookmark under the mouse cursor.

        :param buttons_xy:
        :param color:
        :return:
        """
        for (x, y) in buttons_xy:
            bkgnd_params = {
                'con': 0,
                'x': x,
                'y': y,
                'col': color,
            }
            fgnd_params = {
                'con': 0,
                'x': x,
                'y': y,
                'col': tcod.black,
            }
            tcod.console_set_char_foreground(**fgnd_params)
            tcod.console_set_char_background(**bkgnd_params)

    def _render_borders(self, start_x, color):
        """
        Renders the orders of the tabs bookmarks.

        :param start_x:
        :param color:
        :return:
        """
        end_x = start_x + 4
        for x in range(start_x + 1, end_x):
            tcod.console_put_char_ex(self.console_top_bar, x, 0, chr(205), color, tcod.black)
        tcod.console_put_char_ex(self.console_top_bar, start_x, 0, chr(201), color, tcod.black)
        tcod.console_put_char_ex(self.console_top_bar, end_x, 0, chr(187), color, tcod.black)
        tcod.console_put_char_ex(self.console_top_bar, start_x, 1, chr(186), color, tcod.black)
        tcod.console_put_char_ex(self.console_top_bar, end_x, 1, chr(186), color, tcod.black)

    def _render_bottom_info(self, names):
        """Renders all the info panels on the main console.

        :param names: names of objects under the mouse cursor
        :type names: tuple
        """
        blit_params = {
            'src': self.console_bottom_ui,
            'x': 0,
            'y': 0,
            'w': conf.window_width,
            'h': conf.info_panel_height,
            'dst': 0,
            'xdst': 0,
            'ydst': conf.gui_tabs_height,
            'ffade': 1.0,
            'bfade': 1.0,
        }
        tcod.console_set_default_background(self.console_bottom_ui, tcod.black)
        self._draw_bottom_ui_border()
        self._render_info_under_mouse(names)
        self.render_bookmarks()
        tcod.console_blit(**blit_params)
        tcod.console_clear(self.console_bottom_ui)

    def render_hp_bar(self, player):
        """
        Shows a horizontal HP bar on hte info panel
        """
        off_y = conf.info_panel_height - 2
        self._render_hp_bkgd(off_y)
        self._render_current_hp(player, off_y)
        self._render_values(player, off_y)

    def _render_values(self, player, offset_y):
        """
        Shows the HP values (HP:current/maximum)
        """
        tcod.console_set_default_foreground(self.console_bottom_ui, tcod.white)
        hp_values = "HP:{}/{}".format(player.stats["hp"], player.stats["max_hp"])
        render_params = {
            'con': self.console_bottom_ui,
            'x': conf.window_width / 2,
            'y': offset_y,
            'flag': tcod.BKGND_NONE,
            'alignment': tcod.CENTER,
            'fmt': hp_values
        }
        tcod.console_print_ex(**render_params)

    def _render_hp_bkgd(self, offset_y):
        """
        Shows the background of the HP bar, which is darker than the foreground
        """
        background_color = tcod.dark_grey * tcod.darker_red
        tcod.console_set_default_background(self.console_bottom_ui, background_color)
        hp_bar_params = {
            'con': self.console_bottom_ui,
            'x': 1,
            'y': offset_y,
            'w': int(conf.window_width - 2),
            'h': 1,
            'clr': False,
            'flag': tcod.BKGND_SCREEN,
        }
        tcod.console_rect(**hp_bar_params)

    def _render_current_hp(self, player, offset_y):
        """
        Shows the visual representation of how much HP the player has left
        """
        tcod.console_set_default_background(self.console_bottom_ui, tcod.darker_red)
        current_width = int(float(player.stats["hp"]) / player.stats["max_hp"] * int(conf.window_width - 2))
        current_hp_params = {
            'con': self.console_bottom_ui,
            'x': 1,  # horizontal offset
            'y': offset_y,  # vertical offset
            'w': current_width,
            'h': 1,
            'clr': False,
            'flag': tcod.BKGND_SCREEN,
        }
        if current_width > 0:
            tcod.console_rect(**current_hp_params)

    def _render_info_under_mouse(self, names):
        """
        Blits info panel console to the root console

        :param names: tuple with two elements: (objects_names, tiles_names)
        :type names: tuple
        :return:
        """
        tcod.console_print_rect_ex(self.console_bottom_ui, 1, 1, conf.window_width / 2, 2, tcod.BKGND_NONE, tcod.LEFT, names[0])
        tcod.console_print_ex(self.console_bottom_ui, conf.window_width - 2, 1, tcod.BKGND_NONE, tcod.RIGHT, names[1])

    def _draw_bottom_ui_border(self):
        strip_part = "+-=-"
        strip = "".join([strip_part for x in range((conf.window_width // len(strip_part)) + 1)])
        tcod.console_print_ex(self.console_bottom_ui, 0, 0, tcod.BKGND_NONE, tcod.LEFT, strip)
        tcod.console_print_ex(self.console_bottom_ui, 0, conf.info_panel_height - 1, tcod.BKGND_NONE, tcod.LEFT, strip)

    @staticmethod
    def _blit_and_clear(*, src, ffade: float = 1.0, bfade: float = 0):
        """
        Blits the map on the root console

        :return:
        """
        blit_params = {
            'src': src,
            'x': 0,
            'y': 0,
            'w': conf.camera_width,
            'h': conf.camera_height,
            'dst': 0,
            'xdst': conf.top_bar_width,
            'ydst': conf.top_bar_height,
            'ffade': ffade,
            'bfade': bfade,
        }
        tcod.console_blit(**blit_params)
        tcod.console_clear(src)

    def get_objects_under_cursor(self):
        """
        Grabs the names of all objects under the mouse cursor.

        :return: capitalized string with names separated by a comma
        :rtype: str
        """
        mouse_x, mouse_y = self.mouse.cx + self.cam.x - self.off_x, self.mouse.cy + self.cam.y - self.off_y
        objects_names = [
            game_obj.name for game_obj in self.game_objects
            if (game_obj.x, game_obj.y) == (mouse_x, mouse_y) and self.level[game_obj.x][game_obj.y].explored
        ]
        _names = ', '.join(objects_names)
        return _names.capitalize()

    def get_tiles_under_cursor(self):
        """
        Grabs the names of tiles under the mouse cursor.

        :return: capitalized string with names separated by a comma
        :rtype: str
        """
        mouse_x, mouse_y = self.mouse.cx + self.cam.x - self.off_x, self.mouse.cy + self.cam.y - self.off_y
        tiles_names = [
            self.level[x + self.cam.x][y + self.cam.y].name
            for x in range(conf.camera_width) for y in range(conf.camera_height)
            if (x + self.cam.x, y + self.cam.y) == (mouse_x, mouse_y)
            and self.level[x + self.cam.x][y + self.cam.y].explored
        ]
        _names = ', '.join(tiles_names)
        return _names.capitalize()

    def __render_blinking_cursor(self):
        """
        Change the background of a cell under mouse, in intervals
        """
        adjusted_mouse_x = self.mouse.cx - conf.gui_tabs_width
        adjusted_mouse_y = self.mouse.cy - conf.top_bar_height
        if self.timer / 2.0 <= 0.33:
            tcod.console_set_char_background(self.console_fog, adjusted_mouse_x, adjusted_mouse_y, tcod.lightest_green)
        else:
            tcod.console_set_char_background(self.console_fog, adjusted_mouse_x, adjusted_mouse_y, tcod.red)

    def render_all(self):
        """
        Takes care of drawing, blitting and then flushing the representations of the tiles and objects to the screen.
        """
        self.cam.adjust_camera(self.player.x, self.player.y)
        tcod.console_clear(0)  # 0 is the root console; this clears everything
        self.ticker()
        self.render_map()
        self.render_objects()
        self.render_fog_of_war()
        self.render_info()
        tcod.console_flush()  # render all the blitted stuff on the root console

    def render_info(self):
        """
        renders information in the info panel

        :return:
        """
        names = (self.get_objects_under_cursor(), self.get_tiles_under_cursor())
        self._render_bottom_info(names)
        self.render_hp_bar(self.player)

    def render_map(self):
        """
        Renders the tiles of the map.
        """
        for y in range(conf.camera_width):
            for x in range(conf.camera_height):
                self.render_explored(x, y)
        self._blit_and_clear(self.console_level)

    def render_explored(self, x, y):
        """
        Renders only the explored tiles.

        :param x: x coordinate inside camera view
        :param y: y coordinate inside camera view
        """
        adjusted_x, adjusted_y = x + self.cam.x, y + self.cam.y
        if self.level[adjusted_x][adjusted_y].explored:
            self.level[adjusted_x][adjusted_y].draw(x, y)

    def render_objects(self):
        """
        Renders, that is draws, blits  to console and clears all the objects on the screen.
        """
        self._blit_and_clear(self.console_objects)
        self.draw_all_objects()

    def draw_all_objects(self):
        """
        Draws every object on the objects' console, preparing them for blitting.
        """
        self._draw_items()
        self._draw_npcs()  # drawing items and npcs is separate loops to make sure npc is over an item
        self.player.draw(self.console_objects, self.cam)  # player is drawn last and separate to make sure he's on top

    def _draw_items(self):
        """
        If something isn't an NPC and has been seen once, draw it.
        """
        for game_item in self.game_objects:
            try:
                if not isinstance(game_item, GameCharacter) and self.level[game_item.x][game_item.y].explored:
                    game_item.draw(self.console_objects, self.cam)
            except IndexError:
                pass

    def _draw_npcs(self):
        """
        If something is an NPC or an enemy and inside FieldOfView, draw it
        """
        for npc in self.game_objects:
            if isinstance(npc, GameCharacter) and tcod.map_is_in_fov(self.fov_map, npc.x, npc.y):
                npc.draw(self.console_objects, self.cam)

    def render_fog_of_war(self):
        """
        Renders the fog of war, by recomputing the FieldOfView, and setting transparency of the FOV console accordingly.
        """
        self.recompute_fov()
        for y in range(conf.camera_height):
            for x in range(conf.camera_width):
                self.is_in_fov(x, y)
        self.__render_blinking_cursor()
        self._blit_and_clear(self.console_fog)

    def recompute_fov(self):
        """
        Recomputes the FOV map, based on the player's new location.
        """
        self.make_fov_map()
        tcod.map_compute_fov(
            self.fov_map,
            self.player.x,
            self.player.y,
            conf.sight_radius,
            self.fov_light_walls,
            self.fov_algorithm
        )

    def is_in_fov(self, x, y):
        """
        Checks if the tile is inside the player's FieldOfView.

        If yes, blits a transparent tile on the adjusted x, y on the FOV console.
        Otherwise, blits an opaque tile on the FOV console.

        :type x: int
        :type y: int
        """
        adjusted_x, adjusted_y = x + self.cam.x, y + self.cam.y
        if tcod.map_is_in_fov(self.fov_map, adjusted_x, adjusted_y):
            self.level[adjusted_x][adjusted_y].explored = True
            tcod.console_set_char_background(self.console_fog, x, y, tcod.red)
        else:
            tcod.console_set_char_background(self.console_fog, x, y, tcod.black)

    def make_fov_map(self):
        """
        Creates the FieldOfView map.

        :return:
        """
        for y in range(conf.camera_height):
            for x in range(conf.camera_width):
                adjusted_x, adjusted_y = x + self.cam.x, y + self.cam.y
                fov_properties = {
                    'm': self.fov_map,  # generated map
                    'x': adjusted_x,
                    'y': adjusted_y,
                    'isTrans': self.level[adjusted_x][adjusted_y].transparent,
                    'isWalk': self.level[adjusted_x][adjusted_y].walkable
                }
                tcod.map_set_properties(**fov_properties)

    def ticker(self):
        """
        Keeps track of the game timer
        """
        self.timer += round(tcod.sys_get_last_frame_length(), 2)
        if self.timer >= 1.0:
            self.elapsed += 1
            self.count_elapsed(self.elapsed)
            self.timer = 0

    def count_elapsed(self, elapsed):
        """
        Interprets the time since the game has started; prints values to console
        """
        seconds = elapsed % 60
        minutes = elapsed // 60 % 60
        hours = elapsed // 3600
        print(f"Time spent playing: {hours} h, {minutes} m, {seconds} s.")
