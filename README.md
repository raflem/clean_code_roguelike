# Clean Code Roguelike
A roguelike-like that runs on python 3.7

## Getting Started

## Prerequisites
* your machine can run Python 3.7

## Running the tests
This system doesn't support automated tests (yet, soon^TM), but you can instead:

## Playing the game
So far the interactions are basic:
- use keys as listed in hte launcher to play or exit the game
- use numpad keys to move in cardinal and diagonal directions
- use keys 1-9 on the main keyboard to use/equip inventory items
- use keys = ['t', 'h', 'n', 'w', 'a', 'o', 'g', 'b', 'r'] to dequip items into inventory
- use NUM05 (numpad key 5) to pick up items from the ground
- use the mouse pointer to look around
- use the mouse pointer to drop (l-click) or use/equip/dequip (r-click) items in invenotry and equipment
- ESC goes back to the main menu

### Features:
- tiles you can look at
- goblins you can slay (just walk into them; they are constantly tracking you, too)
- doors you can open (they open automatically, goblins know how to do this, too)
- potions you can pick up and drink
- a ring you can wear
- swords that are just for show
- corpses you can pick up and use (destroys them)
- mouse-look tile highlighting, including inventory
- randomized dungeons coming soon^TM (rooms and corridors generation is there, but no doors, stairs or enemies, yet)

## Built With
Python 3.7

[tcod==11.1.2](https://pypi.org/project/tcod/11.1.2/) - because it's stable

[gitlab](https://gitlab.com) - because my repo can be private for free

## Authors
Rafal Aleksander Lemiec - everything, so far

## License
-empty-

## Acknowledgments
-empty-