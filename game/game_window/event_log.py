import textwrap

from libtcodpy164 import *
from config import conf


class EventLog:
    console = console_new(conf.gui_tabs_width, conf.gui_tabs_height)
    border_colour = light_sepia
    foreground_col = white
    background_col = black

    def __init__(self, events):
        self.events = events
        self.offset_x = conf.gui_tabs_width + conf.camera_width
        self.width = conf.gui_tabs_width
        self.height = conf.gui_tabs_height
        self.title = "+Event Log+"
        self.blit_params = {
            'src': self.console,
            'x': 0,
            'y': 0,
            'w': 0,
            'h': 0,
            'dst': 0,
            'xdst': self.offset_x,
            'ydst': 0,
            'ffade': 1.0,
            'bfade': 1.0,
        }
        self.corners = {
            (0, 0): 201,
            (0, self.height - 1): 200,
            (self.width - 1, 0): 187,
            (self.width - 1, self.height - 1): 188,
        }

    def _draw_borders(self):
        """

        :return:
        """
        self._draw_h_borders()
        self._draw_v_borders()
        self._draw_corners()

    def _draw_v_borders(self):
        """
        Draws vertical borders.

        :return:
        """
        vertical_borders = [(x, y) for x in (0, self.width - 1)
                            for y in range(0, self.height - 1)]
        for (x, y) in vertical_borders:
            console_put_char_ex(self.console, x, y, 186, self.border_colour, self.background_col)

    def _draw_h_borders(self):
        """
        Draws horizontal borders.

        :return:
        """
        horizontal_borders = [(x, y) for x in range(0, self.width - 1)
                              for y in (0, self.height - 1)]
        for (x, y) in horizontal_borders:
            console_put_char_ex(self.console, x, y, 205, self.border_colour, self.background_col)

    def _draw_corners(self):
        """
        Draws the fancy corners of the equipment.

        :return:
        """
        for xy in self.corners:
            x, y = xy
            console_put_char_ex(self.console, x, y, self.corners[xy], self.border_colour, self.background_col)

    def render(self):
        console_clear(self.console)
        self._draw_borders()
        console_set_default_foreground(self.console, self.foreground_col)
        console_print_ex(self.console, conf.gui_tabs_width / 2, 1, BKGND_NONE, CENTER, self.title)
        y = 3
        for event in self.events:
            console_set_default_foreground(self.console, event[1])
            console_print_ex(self.console, 1, y, BKGND_NONE, LEFT, event[0])
            y += 1
        console_blit(**self.blit_params)
