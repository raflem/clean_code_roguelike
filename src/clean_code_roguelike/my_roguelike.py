"""
-+-+-+-+-+-+-+-+-+-+-+-+-+-
Clean Code Roguelike
-+-+-+-+-+-+-+-+-+-+-+-+-+-
version: 0.2.1o
author: RafLem
contact: rafalemiec@gmail.com
gitlab: https://gitlab.com/raflem/clean_code_roguelike/
-+-+-+-+-+-+-+-+-+-+-+-+-+-
launching:
-+-+-+-+-+-+-+-+-+-+-+-+-+-
playing:
    NUMPAD_1 - move/attack/interact SOUTH-WEST
    NUMPAD_2 - move/attack/interact SOUTH
    NUMPAD_3 - move/attack/interact SOUTH-EAST
    NUMPAD_4 - move/attack/interact WEST
    NUMPAD_6 - move/attack/interact EAST
    NUMPAD_7 - move/attack/interact NORTH-WEST
    NUMPAD_8 - move/attack/interact NORTH
    NUMPAD_9 - move/attack/interact NORTH-EAST
    NUMPAD_5 - pick up item on the same tile as Player
    keys 1-9 - use items in inventory as labeled (if equipment, equip item)
    t, h, n, w, a, o, g, b, r - dequip item from equipment to inventory, as labeled
    ESC - return to main menu (loses progress)

-+-+-+-+-+-+-+-+-+-+-+-+-+-
TODO: event log
"""
import tcod
from .game_object import Equipment, HealingPotion, Sword, Ring, Player, Enemy
from .game_window import GameWindow, MenuList

from .dungeon_builder import DungeonBuilder
from config import conf


class Game:
    """
    Everything related to the main game loop
    """
    mouse = tcod.Mouse()
    key = tcod.Key()

    def __init__(self):
        self.game_objects = []
        self.events = []
        self.player = None
        self.window = None

        self.playing = None
        self.main_menu = MenuList("Main Menu", ["Play", "Options", "Exit"])

    def launch_main_menu(self):
        """
        Starts the main menu

        :return:
        """
        self.player = Player(self.game_objects, self.events)
        self.window = GameWindow(game_objects=self.game_objects, mouse=self.mouse, events=self.events)
        tcod.console_print_ex(0, conf.window_width / 2, 1, tcod.BKGND_NONE, tcod.CENTER, "{}\nby RafLem".format(conf.title))
        while not tcod.console_is_window_closed():
            self.__main_menu_render(self.main_menu)
            if self.__main_menu_interact(self.main_menu):
                break

    def __main_menu_render(self, main_menu):
        """
        Renders the main menu (outside of the game).

        :param main_menu:
        :return:
        """
        event_mask = tcod.EVENT_KEY_RELEASE | tcod.EVENT_MOUSE
        tcod.sys_check_for_event(event_mask, self.key, self.mouse)
        main_menu.blit()
        tcod.console_flush()

    def __main_menu_interact(self, main_menu):
        """
        Handles player's input in the main menu. Unfortunately this cannot be more generic

        :param main_menu:
        :type main_menu: MenuList
        :return: True if exit command is sent
        """
        choices = main_menu.options
        try:  # error handling prevents failing before the key is pressed
            choice = choices[self.key.c - ord('a')]
            self.__play_game(choice)
            self.__options(choice)
            if choice == 'Exit':
                return True
        except IndexError:
            pass

    @staticmethod
    def __options(choice):
        if choice == 'Options':
            pass

    def __play_game(self, choice):
        """
        Starts the game proper.

        :param choice:
        :return:
        """
        if choice == 'Play':
            self.playing = True
            self.play()

    # noinspection PyTypeChecker
    def play(self):
        """
        Main playing loop.
        """
        dungeon_builder = DungeonBuilder(self.window.console_level, self.window.level, self.game_objects, self.events)
        dungeon_builder.build_tower(self)
        # dungeon_builder.build_random(5, self)
        while not tcod.console_is_window_closed() and self.playing:
            self.__main_loop()
            if self.key.vk == tcod.KEY_ESCAPE:
                self.__return_to_launch()

    def __return_to_launch(self):
        """
        Returns to the main launch view.

        :return:
        """
        self.playing = False
        self.game_objects = []
        tcod.console_clear(0)
        self.launch_main_menu()

    def __main_loop(self):
        """
        The main gaming loop. Provides keyboard and mouse event mask and checking for said events.

        :return:
        """
        event_mask = tcod.EVENT_KEY_PRESS | tcod.EVENT_MOUSE
        tcod.sys_check_for_event(event_mask, self.key, self.mouse)
        self.window.render_all()
        self.action_handler()

    def action_handler(self):
        """
        Handles the basic player interaction with the game, like using equipment and inventory and moving around.

        :return:
        """
        self.inventory_interaction()
        self.equipment_interaction()
        player_move = self.player.interaction(self.key)
        if player_move:
            self.handle_NPCs()

    def inventory_interaction(self):
        """
        Allows to use, equip, or drop items in inventory with the keyboard.
        Use keys 1-9 on the main keayboard to use an item or to equip an equipment item.

        :return:
        """
        items_keys = [_item for _item in self.player.inventory if self.player.inventory[_item] is not None]
        adj_key_code = self.key.vk - 25
        if adj_key_code in items_keys:
            _item = self.player.inventory[adj_key_code]
            self._use_or_equip(_item)

    def _use_or_equip(self, _item):
        """
        If an item is an equipment piece, equip it; otherwise use the item.

        :param _item:
        :return:
        """
        if not isinstance(_item, Equipment):
            _item.use(self.player)
        else:
            _item.equip(self.player)

    def equipment_interaction(self):
        eqp_keys = {
            ord('t'): 0,
            ord('h'): 1,
            ord('n'): 2,
            ord('w'): 3,
            ord('a'): 4,
            ord('o'): 5,
            ord('g'): 6,
            ord('b'): 7,
            ord('r'): 8,
        }
        if self.key.c in eqp_keys:
            print(self.key.vk, self.key.c)
            eqp_index = eqp_keys[self.key.c]
            equipped_item = self.player.equipment[eqp_index]
            if equipped_item:
                equipped_item.dequip(self.player)

    def handle_NPCs(self):
        """
        Handles NPCs behavior
        """
        for enemy in self.game_objects:
            if isinstance(enemy, Enemy):
                enemy.follow_player(self.player)

    def spawn_player(self, x, y):
        """
        Creates and spawns the player
        """
        self.player.spawn((x, y), self.window.level)
        HealingPotion().add_to_inventory(self.player.inventory, self.game_objects)
        Ring().add_to_inventory(self.player.inventory, self.game_objects)
        Sword().add_to_inventory(self.player.inventory, self.game_objects)


if __name__ == '__main__':
    Game().launch_main_menu()
