import tcod
from .tile import Tile


class Floor(Tile):
    """
    The default floor tile
    """
    name = 'floor'
    char = 46

    def __init__(self, console):
        """

        :param console: libtcod console onto which the tile is blit
        """
        Tile.__init__(self, console=console)
        self.walkable = True
        self.transparent = True
        self.foreground_color = tcod.green


class StoneFloor(Floor):
    name = 'stone floor'

    def __init__(self, console):
        """

        :param console: libtcod console onto which the tile is blit
        """
        Floor.__init__(self, console=console)
        self.foreground_color = tcod.lighter_grey
