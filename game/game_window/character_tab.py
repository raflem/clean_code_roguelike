from gui_tab import *


class CharacterGui(GuiTab):
    """
    Character sheet tab, containing information about player's stats.
    """
    def __init__(self, player, mouse):
        GuiTab.__init__(self, player, mouse)
        self.corners = {
            (self.offset_x, conf.gui_tabs_height - 1): 200,
            (self.offset_x + 4, self.offset_y ): 200,
            (self.offset_x + conf.gui_tabs_width - 1, self.offset_y): 187,
            (self.offset_x + conf.gui_tabs_width - 1, conf.gui_tabs_height - 1): 188,
        }
        self.border_colour = green
        self.tag = "cha"
        self.range = range(self.offset_x, self.offset_x + 5)

    def _draw_stats(self):
        y = 5
        for stat in self.player.stats:
            if 'hp' not in stat:
                _value = self.player.stats[stat]
                console_print(self.console, 1, y, '{}: {}'.format(stat, _value))
                y += 1

    def render(self):
        """
        Blits the character tab to the root console.
        """
        self._draw_stats()
        self.blit(self.range, self.tag)
