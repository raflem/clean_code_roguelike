from gui_tab import *


class EquipmentGui(GuiTab):
    """
    Equipment sheet, containing information on items equipped by the player.
    """
    def __init__(self, player, mouse):
        GuiTab.__init__(self, player, mouse)
        self.corners = {
            (self.offset_x, conf.top_bar_height): 201,
            (self.offset_x, conf.gui_tabs_height - 1): 200,
            (self.offset_x + conf.gui_tabs_width - 5, conf.top_bar_height): 188,
            (self.offset_x + conf.gui_tabs_width - 1, conf.gui_tabs_height - 1): 188,
        }
        self.border_colour = yellow
        self.tag = "eqp"
        self.range = range(self.offset_x + 8, conf.gui_tabs_width)

    def _item_interaction(self, clicked_item):
        """
        Right click will dequip the item (move it to the inventory), left click will drop it.

        :param clicked_item:
        :return: null
        """
        if self.mouse.rbutton_pressed:
            clicked_item.dequip(self.player)
        elif self.mouse.lbutton_pressed:
            clicked_item.drop(self.player)

    def __draw_eqp(self, equipped_item):
        """
        Draws the existing item's character in the equipment.

        :param equipped_item:
        :type equipped_item: item.Equipment
        """
        if equipped_item:
            item_properties = {
                'con': self.console,
                'x': equipped_item.eqp_x + self.offset_x,
                'y': equipped_item.eqp_y + self.offset_y,
                'c': equipped_item.char,
                'fore': equipped_item.color,
                'back': self.background,
            }
            console_put_char_ex(**item_properties)
            self._highlight_item(equipped_item)

    def _render_equipment_items(self):
        """
        Renders the items inside the player's equipment.
        """
        for equipment in self.player.equipment.values():
            self.__draw_eqp(equipment)

    def _render_equipment_slots(self):
        """
        Renders the slots/holders for the equipment items.
        """
        equipment_slots = {
            0: 'trk',
            1: 'hlm',
            2: 'nkl',
            3: 'wep',
            4: 'arm',
            5: 'off',
            6: 'glv',
            7: 'bot',
            8: 'rng',
        }
        item_id = 0
        for coord in conf.inv_xy:
            new_x, new_y = coord
            eqp_holders_params = {
                'con': self.console,
                'x': new_x + self.offset_x,
                'y': new_y + self.offset_y ,
                'flag': BKGND_NONE,
                'alignment': CENTER,
                'fmt': '{ }',
            }
            eqp_params = {
                'con': self.console,
                'x': new_x + self.offset_x,
                'y': new_y + self.offset_y + 1,
                'flag': BKGND_NONE,
                'alignment': CENTER,
                'fmt': equipment_slots[item_id].capitalize(),
            }
            console_print_ex(**eqp_holders_params)
            console_print_ex(**eqp_params)
            item_id += 1

    def _render_eqp_name(self):
        _eqp_name = self.get_eqp_name_under_mouse()
        _eqp_info_params = {
            'con': self.console,
            'x': 1,
            'y': conf.gui_tabs_height - 4,
            'w': conf.gui_tabs_width - 2,
            'h': 3,
            'flag': BKGND_NONE,
            'alignment': LEFT,
            'fmt': _eqp_name
        }
        console_print_rect_ex(**_eqp_info_params)

    def get_eqp_name_under_mouse(self):
        """
        Creates a single-item list with the name of the equipped item that's being highlighted

        :return: capitalized string with names separated by a comma
        :rtype: str
        """
        equipment_names = [
            _eqp.name for _eqp in self.player.equipment.values()
            if _eqp and (_eqp.eqp_x, _eqp.eqp_y) == (self.mouse.cx - self.offset_x, self.mouse.cy - self.offset_y)
        ]
        _eqp_name = ''.join(equipment_names)
        return _eqp_name.capitalize()

    def render(self):
        """
        Blits the inventory panel to the root console.

        :return :
        """
        self._render_equipment_slots()
        self._render_equipment_items()
        self._render_eqp_name()
        self.blit(self.range, self.tag)
